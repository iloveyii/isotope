<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BS 3 - Isotope</title>
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.css">
    <style>
        body {
            background-color:#fcfcfc;
        }

        .row {
            margin-left:0;
            margin-right:0;
        }

        .col-md-3 {

        }

        .btn-warning {
            margin-left: 5px;
        }

        /* make the well swell */
        .well {
            border-color:#eee;
        }

        .item h4 a {
            color: #333;
        }

        .item img.image {
            max-width: 100%;
            margin-top: 9px;
        }

        .info {
            padding:4px;
        }

        /* Isotope Transitions
        ------------------------------- */
        .isotope,
        .isotope .item {
            -webkit-transition-duration: 0.8s;
            -moz-transition-duration: 0.8s;
            -ms-transition-duration: 0.8s;
            -o-transition-duration: 0.8s;
            transition-duration: 0.8s;
        }

        .isotope {
            -webkit-transition-property: height, width;
            -moz-transition-property: height, width;
            -ms-transition-property: height, width;
            -o-transition-property: height, width;
            transition-property: height, width;
        }

        .isotope .item {
            -webkit-transition-property: -webkit-transform, opacity;
            -moz-transition-property:    -moz-transform, opacity;
            -ms-transition-property:     -ms-transform, opacity;
            -o-transition-property:         top, left, opacity;
            transition-property:         transform, opacity;
        }


        /* responsive media queries */

        @media (max-width: 768px) {
            header h1 small {
                display: block;
            }

            header div.description {
                padding-top: 9px;
                padding-bottom: 4px;
            }

            .isotope .item {
                position: static ! important;
                -webkit-transform: translate(0px, 0px) ! important;
                -moz-transform: translate(0px, 0px) ! important;
                transform: translate(0px, 0px) ! important;
            }
        }

        /* CUSTOMIZE THE NAVBAR
        -------------------------------------------------- */

        /* Special class on .container surrounding .navbar, used for positioning it into place. */
        .navbar-wrapper {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            z-index: 10;
        }



        /* CUSTOMIZE THE CAROUSEL
        -------------------------------------------------- */

        /* Carousel base class */
        .carousel {
            margin-bottom: 60px;
        }
        /* Since positioning the image, we need to help out the caption */
        .carousel-caption {
            z-index: 10;
        }

        /* Declare heights because of positioning of img element */
        .carousel .item {
            height: 500px;
            background-color:#bbb;
        }
        .carousel img {
            position: absolute;
            top: 0;
            left: 0;
            min-width: 100%;
            height: 500px;
        }


    </style>

   </head>
<body>


<div class="navbar-wrapper">
    <div class="container">
        <div class="navbar navbar-inverse navbar-static-top">

            <div class="navbar-header">
                <a class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a class="navbar-brand" href="#">Bootstrap 3</a>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#" target="ext">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li><a href="#">One more separated link</a></li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div><!-- /container -->
</div><!-- /navbar wrapper -->
<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img src="http://lorempixel.com/1500/600/abstract/2">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Bootstrap 3 Carousel Layout</h1>
                    <pthis is="" an="" example="" layout="" with="" carousel="" that="" uses="" the="" bootstrap="" 3="" styles.<="" small=""><p></p>
                    <p><a class="btn btn-lg btn-primary" href="http://getbootstrap.com">Learn More</a>
                    </p></pthis></div>
            </div>
        </div>
        <div class="item">
            <img src="http://lorempixel.com/1500/600/abstract/3">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Changes to the Grid</h1>
                    <p>Bootstrap 3 still features a 12-column grid, but many of the CSS class names have completely changed.</p>
                    <p><a class="btn btn-large btn-primary" href="#">Learn more</a></p>
                </div>
            </div>
        </div>
        <div class="item">
            <img src="http://placehold.it/1500X500">
            <div class="container">
                <div class="carousel-caption">
                    <h1>Percentage-based sizing</h1>
                    <p>With "mobile-first" there is now only one percentage-based grid.</p>
                    <p><a class="btn btn-large btn-primary" href="#">Browse gallery</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>
</div>
<!-- /.carousel -->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h1>Bootstrap and Isotope</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <button id="masonry" class="btn btn-success">Masonry</button>
            <button id="packery" class="btn btn-default">Packery</button>
            <button id="fitRows" class="btn btn-danger">FitRows</button>
        </div>
        <div class="col-md-2">
            <span><strong id="mode">PACKERY</strong>  |</span>
        </div>

        <div class="col-md-2">
            <span>| <strong id="filter">ALL</strong></span>
        </div>

        <div class="col-md-4" id="filters">

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <hr>
        </div>
    </div>


    <div id="posts" class="row">
        <?php
            $classes = ['tree', 'water', 'flower'];

            $files = scandir(__DIR__ . '/img/');
            $count = 1;

            foreach ($files as $file) :
                if( ! in_array($file, ['.', '..'])) :
        ?>

            <div id="1" class="item col-md-3 col-sm-4 col-xs-12 col-lg-2 <?= $classes[rand(0, count($classes) - 1)]?>">
                <div class="well">
                    <h4><a href="#" target="_blank"><?=substr($file, 0, 10)?></a></h4>

                    <img class="thumbnail img-responsive" src="/img/<?=$file?>">
                    <div class="info">
                        <span class="badge">#</span>
                        <span class="badge"><?=sprintf("%02s",$count++)?></span>
                    </div>
                </div>
            </div>

        <?php endif; ?>
        <?php endforeach; ?>
    </div>
</div>
<footer class="text-center"><a href="#">Isotope</a>
</footer>

<script src="/js/jquery.js"></script>
<script src="/js/imagesloaded.pkgd.js"></script>
<script src="/js/bootstrap.js"></script>

<!-- //cdn.jsdelivr.net/isotope/1.5.25/jquery.isotope.min.js : this works without imagesloaded js but horizontalOrder of isotope won't work -->
<!-- //cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.4/isotope.pkgd.min.js : for this you need imagesloaded package above (may be latest jquery removed it) -->
<script src="/js/isotope.pkgd.js"></script>
<script src="/js/packery-mode.pkgd.js"></script>

<script>

        $('#posts').imagesLoaded(function () {

            console.log('All images loaded');

            $('#posts').isotope({
                itemSelector : '.item',
                layoutMode : 'packery',
                masonry : {
                    columnWidth : 5,
                    horizontalOrder : true,
                }
            });
        });

</script>

<script>
    $('.btn').on('click', function () {
        var mode = this.id;
        $('#posts').isotope('destroy');
        $('#mode').html(mode.toUpperCase());

        $('#posts').isotope({
            itemSelector : '.item',
            layoutMode : mode,
            masonry : {
                columnWidth : 5,
                horizontalOrder : true,
            }
        });
    })
</script>

<script>
    var filters = <?= json_encode($classes) ?>;

    var applyFilter = function(filter) {
        console.log('Applied filter', filter);
        $('#filter').html(filter.toUpperCase());
        $('#posts').isotope({filter: '.'+filter});
    }
    
    var filterButtons = filters.map(function (filter) {
        console.log(filter);
        var button = $('<button />', { text : filter.toUpperCase(), class: 'btn btn-warning pull-right', click: applyFilter.bind(null, filter)});

        return button;
    }, applyFilter);

    console.log(filterButtons);

    filterButtons.forEach(function(filterButton){
        console.log(filterButton);
        $('#filters').append(filterButton);
    });

</script>


</body>
</html>

